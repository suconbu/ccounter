﻿namespace ccounter
{
	partial class AboutFrom
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && (components != null) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutFrom));
			this.uiPictureBox = new System.Windows.Forms.PictureBox();
			this.uiVersionInfo = new System.Windows.Forms.Label();
			this.uiOkButton = new System.Windows.Forms.Button();
			this.uiChangeLog = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.uiPictureBox)).BeginInit();
			this.SuspendLayout();
			// 
			// uiPictureBox
			// 
			this.uiPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("uiPictureBox.Image")));
			this.uiPictureBox.Location = new System.Drawing.Point(12, 9);
			this.uiPictureBox.Name = "uiPictureBox";
			this.uiPictureBox.Size = new System.Drawing.Size(64, 64);
			this.uiPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.uiPictureBox.TabIndex = 0;
			this.uiPictureBox.TabStop = false;
			// 
			// uiVersionInfo
			// 
			this.uiVersionInfo.Location = new System.Drawing.Point(82, 9);
			this.uiVersionInfo.Name = "uiVersionInfo";
			this.uiVersionInfo.Size = new System.Drawing.Size(129, 64);
			this.uiVersionInfo.TabIndex = 1;
			this.uiVersionInfo.Text = "Version info\r\n";
			this.uiVersionInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// uiOkButton
			// 
			this.uiOkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.uiOkButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.uiOkButton.Location = new System.Drawing.Point(136, 191);
			this.uiOkButton.Name = "uiOkButton";
			this.uiOkButton.Size = new System.Drawing.Size(75, 23);
			this.uiOkButton.TabIndex = 3;
			this.uiOkButton.Text = "OK";
			this.uiOkButton.UseVisualStyleBackColor = true;
			this.uiOkButton.Click += new System.EventHandler(this.uiOkButton_Click);
			// 
			// uiChangeLog
			// 
			this.uiChangeLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.uiChangeLog.Location = new System.Drawing.Point(12, 79);
			this.uiChangeLog.Multiline = true;
			this.uiChangeLog.Name = "uiChangeLog";
			this.uiChangeLog.ReadOnly = true;
			this.uiChangeLog.Size = new System.Drawing.Size(199, 104);
			this.uiChangeLog.TabIndex = 2;
			// 
			// AboutFrom
			// 
			this.AcceptButton = this.uiOkButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.uiOkButton;
			this.ClientSize = new System.Drawing.Size(224, 222);
			this.Controls.Add(this.uiOkButton);
			this.Controls.Add(this.uiChangeLog);
			this.Controls.Add(this.uiVersionInfo);
			this.Controls.Add(this.uiPictureBox);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(240, 200);
			this.Name = "AboutFrom";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
			this.Text = "About ccounter";
			this.Load += new System.EventHandler(this.AboutFrom_Load);
			((System.ComponentModel.ISupportInitialize)(this.uiPictureBox)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox uiPictureBox;
		private System.Windows.Forms.Label uiVersionInfo;
		private System.Windows.Forms.Button uiOkButton;
		private System.Windows.Forms.TextBox uiChangeLog;
	}
}