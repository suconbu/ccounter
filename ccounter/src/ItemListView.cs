﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;

namespace ccounter
{
	class ItemListView : ListView
	{
		//
		// Public properties
		//

		public class ProgressChangedEventArgs : EventArgs
		{
			public enum ProgressState
			{
				Working,
				Finished,
				Cancelled,
			}
			public ProgressChangedEventArgs( int min, int max, int current, ProgressState state = ProgressState.Working )
			{
				this.Min = min;
				this.Max = max;
				this.Current = current;
				this.State = state;
			}
			public int Current { get; private set; }
			public int Min { get; private set; }
			public int Max { get; private set; }
			public ProgressState State { get; private set; }
		}

		public event ListViewVirtualItemsSelectionRangeChangedEventHandler SelectionChanged = delegate { };
		public event EventHandler<ProgressChangedEventArgs> LoadingProgressChanged = delegate { };
		public event EventHandler LoadingFinished = delegate { };
		public List<CountableItem> ViewItemList { get; private set; }
		public List<CountableItem> SelectedItemList { get; private set; }
		public List<DirectoryItem> DirectoryItemList { get; private set; }
		public string FileNameFilter { get; set; }

		//
		// Public methods
		//

		public ItemListView()
		{
			this.SetStyle( ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true );
			this.SetStyle( ControlStyles.EnableNotifyMessage, true );

			this.View = View.Details;
			this.VirtualMode = true;
			this.HideSelection = false;
			this.FullRowSelect = true;
			this.MultiSelect = true;
			this.GridLines = true;

			foreach( var p in ItemListView.ColumnParameters )
			{
				this.Columns.Add( this.CreateColumnHeader( p.Name, p.Text, p.TextAlign, p.Width ) );
			}

			this.ItemSelectionChanged += ItemListView_ItemSelectionChanged;
			this.VirtualItemsSelectionRangeChanged += ItemListView_VirtualItemsSelectionRangeChanged;
			this.SelectedIndexChanged += ListView_SelectedIndexChanged;
			this.RetrieveVirtualItem += ItemListView_RetrieveVirtualItem;
			this.CacheVirtualItems += ItemListView_CacheVirtualItems;
			this.ColumnClick += ItemListView_ColumnClick;
			this.MouseDown += ItemListView_MouseDown;
			this.MouseUp += ItemListView_MouseUp;
			this.MouseMove += ItemListView_MouseMove;

			this.ViewItemList = new List<CountableItem>();
			this.SelectedItemList = new List<CountableItem>();
			this.DirectoryItemList = new List<DirectoryItem>();
			this.countingItemQueue = new Queue<CountableItem>();
			this.columnSortInfo.Index = -1;
			this.columnSortInfo.Order = SortOrder.None;
		}

		public void SelectAll()
		{
			if( this.isLoading )
			{
				// 読み込み中は全選択できません
				return;
			}

			int count = this.VirtualListSize;

			this.selectionChangedEventBlocking = true;
			for( int i = 0; i < count; i++ )
			{
				this.SelectedIndices.Add( i );
			}
			this.selectionChangedEventBlocking = false;

			if( count > 0 )
			{
				this.ReflectSelection( 0, count - 1, true );
			}
			else
			{
				this.ReflectSelection( -1, -1, false );
			}
		}

		// 表示対象のディレクトリを設定
		public void SetDirectories( string[] directoryPaths )
		{
			var newItemList = new List<CountableItem>();

			//// 文字列順に並んだリストを用意
			//var sortedPathList = new List<string>();
			//sortedPathList.AddRange( directoryPaths );
			//sortedPathList.Sort();

			if( directoryPaths != null )
			{
				int itemIndex = 0;
				this.DirectoryItemList.Clear();
				foreach( string dpath in directoryPaths )
				{
					// 自分の上位ディレクトリがいたらそこに所属させる
					string treePath = DirectoryTreeNode.GetTreePath( dpath );
					var parent = this.DirectoryItemList.FindLast( ( item ) =>
						treePath.Contains( DirectoryTreeNode.GetTreePath( item.CombiPath ) ) );

					var ditem = new DirectoryItem( itemIndex, dpath, parent );
					this.DirectoryItemList.Add( ditem );
					newItemList.Add( ditem );
					itemIndex++;

					foreach( var filePath in Util.GetFilesInDirectory( dpath, this.FileNameFilter ) )
					{
						string fileName = Path.GetFileName( filePath );
						string ext = Path.GetExtension( fileName );

						newItemList.Add( new FileItem( itemIndex, filePath, ext, ditem ) );
						itemIndex++;
					}
				}
			}

			if( this.ViewItemList.Count > 0 )
			{
				this.Columns["Path"].AutoResize( ColumnHeaderAutoResizeStyle.ColumnContent );
				this.Columns["Ext"].AutoResize( ColumnHeaderAutoResizeStyle.ColumnContent );
			}

			CountItemsAsync( newItemList );

			this.SelectedIndices.Clear();
			this.OnSelectedIndexChanged( new EventArgs() );
			//this.Refresh();
		}

		//
		// Protected methods
		//

		protected override void OnNotifyMessage( Message m )
		{
			if( m.Msg == 0x0014 )	// WM_ERASEBKGND
			{
				return;
			}
			base.OnNotifyMessage( m );
		}

		//
		// Private fields
		//

		// 列ヘッダ
		internal class ColumnParameter
		{
			public int Index { get; set; }
			public string Name { get; set; }
			public string Text { get; set; }
			public HorizontalAlignment TextAlign { get; set; }
			public int Width { get; set; }
			public bool IsNumeric { get; set; }
			public bool IsCountData { get; set; }
		}

		internal static readonly ColumnParameter[] ColumnParameters =
		{
			new ColumnParameter { Index = 0,	Name = "Path",		Text = "Path",		TextAlign = HorizontalAlignment.Left,	Width = 200,	IsNumeric = false,	IsCountData = false	},
			new ColumnParameter { Index = 1,	Name = "Ext",		Text = "Ext.",		TextAlign = HorizontalAlignment.Left,	Width = 40,		IsNumeric = false,	IsCountData = false	},
			new ColumnParameter { Index = 2,	Name = "All",		Text = "All",		TextAlign = HorizontalAlignment.Right,	Width = 80,		IsNumeric = true,	IsCountData = true	},
			new ColumnParameter { Index = 3,	Name = "Enable",	Text = "Enable",	TextAlign = HorizontalAlignment.Right,	Width = 80,		IsNumeric = true,	IsCountData = true	},
			new ColumnParameter { Index = 4,	Name = "Blank",		Text = "Blank",		TextAlign = HorizontalAlignment.Right,	Width = 80,		IsNumeric = true,	IsCountData = true	},
			new ColumnParameter { Index = 5,	Name = "Comment",	Text = "Comment",	TextAlign = HorizontalAlignment.Right,	Width = 80,		IsNumeric = true,	IsCountData = true	},
			new ColumnParameter { Index = 6,	Name = "Effective",	Text = "Effective",	TextAlign = HorizontalAlignment.Right,	Width = 80,		IsNumeric = true,	IsCountData = true	},
		};

		// 列ヘッダラベルから列インデックスを取得
		internal static ColumnParameter FindColumnParameter( string name )
		{
			return ItemListView.ColumnParameters.First( ( c ) => c.Name == name );
		}

		// 進捗率更新通知の頻度(*回に1回)
		private static int kProgressNotifyInterval = 9;

		// 読み込み中並び替え実施の頻度(*回に1回)
		private static int kSortIntervalOnLoading = 50;

		// 行数情報読み込みスレッド個数
		private static int kLoaderTaskNum = 4;

		// 選択状態変更通知を抑制中か
		private bool selectionChangedEventBlocking = false;

		// ソート列情報
		private struct ColumnSortInfo
		{
			public int Index;
			public SortOrder Order;
		}
		private ColumnSortInfo columnSortInfo;

		// 行数情報読み込みスレッド関係
		private Task[] loaderSubTasks = new Task[ItemListView.kLoaderTaskNum];
		private Task loaderMainTask;
		private CancellationTokenSource cancelTokenSource;
		private object countingItemQueueLock = new object();
		private bool isLoading { get { return this.loaderMainTask != null && this.loaderMainTask.Status == TaskStatus.Running; } }
		// 読み込み待ちキュー
		private Queue<CountableItem> countingItemQueue;

		// 選択制御
		private bool mouseDowned = false;
		private bool mouseDownedItemSelected = false;
		private Point prevMousePoint;

		//
		// Private methods
		//

		private ColumnHeader CreateColumnHeader( string name, string text, HorizontalAlignment textAlign, int width )
		{
			var column = new ColumnHeader();
			column.Name = name;
			column.Text = text;
			column.TextAlign = textAlign;
			column.Width = width;
			return column;
		}

		void ItemListView_MouseMove( object sender, MouseEventArgs e )
		{
			if( this.mouseDowned )
			{
				int divisionCount = (int)(Util.GetDistance( this.prevMousePoint, e.Location ) / this.FontHeight) + 1;
				//Console.WriteLine( "[ItemListView_MouseMove] prev:{0} now:{1}", this.prevMousePoint.ToString(), e.Location.ToString() );

				// 素早く移動すると間が空いてしまうので補間してあげる
				for( int i = 1; i <= divisionCount; i++ )
				{
					int x = this.prevMousePoint.X + (e.X - this.prevMousePoint.X) * i / divisionCount;
					int y = this.prevMousePoint.Y + (e.Y - this.prevMousePoint.Y) * i / divisionCount;
					//Console.WriteLine( "[ItemListView_MouseMove]   {{{0},{1}}}", x, y );

					var item = this.GetItemAt( x, y );
					if( item != null )
					{
						if( this.mouseDownedItemSelected )
						{
							// 選択解除
							this.SelectedIndices.Remove( item.Index );
						}
						else
						{
							// 選択
							this.SelectedIndices.Add( item.Index );
							//Console.WriteLine( "[ItemListView_MouseMove] SelectedIndices.Count:{0}", this.SelectedIndices.Count );
						}
					}
				}
			}
		}

		void ItemListView_MouseUp( object sender, MouseEventArgs e )
		{
			if( e.Button == MouseButtons.Left )
			{
				this.mouseDowned = false;
			}
		}

		void ItemListView_MouseDown( object sender, MouseEventArgs e )
		{
			if( e.Button == MouseButtons.Left )
			{
				var item = this.GetItemAt( e.X, e.Y );
				if( item != null )
				{
					this.mouseDownedItemSelected = item.Selected;
					this.mouseDowned = true;
					this.prevMousePoint = e.Location;
				}
			}
		}

		// 列ヘッダがクリックされた時
		private void ItemListView_ColumnClick( object sender, ColumnClickEventArgs e )
		{
			//if( this.isLoading )
			//{
			//	// 読み込み中はソートできません
			//	// 読み込みスレッドはthis.ViewItemList[0]から順番に処理していくので
			//	return;
			//}

			if( e.Column == this.columnSortInfo.Index )
			{
				// 同じ列をクリックしたら「降順->昇順->ソート解除」の順で切り替え
				this.columnSortInfo.Order =
					(this.columnSortInfo.Order == SortOrder.Descending) ? SortOrder.Ascending :
					(this.columnSortInfo.Order == SortOrder.Ascending) ? SortOrder.None :
					SortOrder.Descending;
			}
			else
			{
				// 初回または違う列をクリックしたらまずは降順でソート
				if( this.columnSortInfo.Index >= 0 )
				{
					// 前の列ヘッダのアイコンを消去
					// 列ヘッダのアイコンを消すにはTextAlignの再設定が必要
					// 参考：http://qiita.com/kobake@github/items/efe041f779e52bc471cb
					var prevSortedColumn = this.Columns[this.columnSortInfo.Index];
					prevSortedColumn.ImageIndex = -1;
					prevSortedColumn.TextAlign = prevSortedColumn.TextAlign;
				}
				this.columnSortInfo.Index = e.Column;
				this.columnSortInfo.Order = SortOrder.Descending;
			}
			var clickedColumn = this.Columns[e.Column];
			clickedColumn.ImageIndex =
				(this.columnSortInfo.Order == SortOrder.Ascending) ? 4 :
				(this.columnSortInfo.Order == SortOrder.Descending) ? 5 :
				-1;
			clickedColumn.TextAlign = clickedColumn.TextAlign;

			this.columnSortInfo.Index = (this.columnSortInfo.Order == SortOrder.None) ? -1 : this.columnSortInfo.Index;

			if( this.columnSortInfo.Order == SortOrder.None )
			{
				// ソート解除時は最初の順番に並べる
				ItemListView.SortItems( this.ViewItemList, -1, true );
			}
			else
			{
				ItemListView.SortItems( this.ViewItemList, this.columnSortInfo.Index, this.columnSortInfo.Order == SortOrder.Ascending );
			}
			this.Refresh();
		}

		// SelectedIndices.Clear()に対してSelectionChangedを発行するために用意
		private void ListView_SelectedIndexChanged( object sender, EventArgs e )
		{
			if( this.SelectedIndices.Count <= 0 )
			{
				this.ReflectSelection( -1, -1, false );
			}
		}

		private void ItemListView_ItemSelectionChanged( object sender, ListViewItemSelectionChangedEventArgs e )
		{
			if( !this.selectionChangedEventBlocking )
			{
				this.ReflectSelection( e.ItemIndex, e.ItemIndex, e.IsSelected );
			}
		}

		private void ItemListView_VirtualItemsSelectionRangeChanged( object sender, ListViewVirtualItemsSelectionRangeChangedEventArgs e )
		{
			if( !this.selectionChangedEventBlocking )
			{
				this.ReflectSelection( e.StartIndex, e.EndIndex, e.IsSelected );
			}
		}

		private void ItemListView_CacheVirtualItems( object sender, CacheVirtualItemsEventArgs e )
		{
			for( int i = e.StartIndex; i <= e.EndIndex; i++ )
			{
				this.ViewItemList[i].ReflectCountDataToControl();
			}
		}

		private void ItemListView_RetrieveVirtualItem( object sender, RetrieveVirtualItemEventArgs e )
		{
			e.Item = this.ViewItemList[e.ItemIndex];
		}

		private void CountItemsAsync( List<CountableItem> newItemList )
		{
			// まずは裏読みに中止を掛けておく
			this.CancelCountItems();

			// 計測結果の再利用のために現在のリストのコピーを取っておく
			var sw = System.Diagnostics.Stopwatch.StartNew();
			var previousItemList = this.ViewItemList.ToList();
			sw.Stop();
			Console.WriteLine( "this.Items.ToList():{0}[us]", sw.ElapsedTicks * 1000L * 1000L / System.Diagnostics.Stopwatch.Frequency );

			this.ViewItemList = newItemList;
			this.VirtualListSize = this.ViewItemList.Count;

			// 裏読み開始

			this.cancelTokenSource = new CancellationTokenSource();
			var cancelToken = this.cancelTokenSource.Token;

			var previousMainTask = this.loaderMainTask;

			this.loaderMainTask = Task.Factory.StartNew( () =>
			{
				// 前の読み込みが止まり切るまで待つ
				if( previousMainTask != null )
				{
					try
					{
						previousMainTask.Wait();
					}
					catch( AggregateException )
					{
						;
					}
				}

				// 今回分をキューに設定
				this.SetCountingItemQueue( newItemList, previousItemList );
				int queuedNum = this.countingItemQueue.Count;

				// 進捗表示をリセット
				this.Invoke( (MethodInvoker)(() =>
					this.LoadingProgressChanged( this, new ProgressChangedEventArgs( 0, queuedNum, 0 ) )) );

				// ここからワーカースレッドを使って読み込み開始
				for( int taskIndex = 0; taskIndex < ItemListView.kLoaderTaskNum; taskIndex++ )
				{
					this.loaderSubTasks[taskIndex] = Task.Factory.StartNew( () =>
						this.CountQueuedItems( queuedNum, cancelToken ), cancelToken );
				}

				try
				{
					Task.WaitAll( this.loaderSubTasks );

					// 全部読み切りました

					this.Invoke( (MethodInvoker)(() =>
						this.LoadingProgressChanged( this, new ProgressChangedEventArgs( 0, queuedNum, queuedNum ) )) );

					if( this.columnSortInfo.Order != SortOrder.None )
					{
						ItemListView.SortItems( this.ViewItemList, this.columnSortInfo.Index, this.columnSortInfo.Order == SortOrder.Ascending );
					}

					this.Invoke( (MethodInvoker)(() =>
					{
						foreach( var item in this.ViewItemList )
						{
							item.ReflectCountDataToControl();
						}

						this.Refresh();

						this.LoadingProgressChanged( this, new ProgressChangedEventArgs( 0, queuedNum, queuedNum, ProgressChangedEventArgs.ProgressState.Finished ) );
					}) );
				}
				catch( AggregateException )
				{
					// キャンセルされました
					// https://msdn.microsoft.com/ja-jp/library/dd997396(v=vs.100).aspx
					this.Invoke( (MethodInvoker)(() => this.LoadingProgressChanged( this, new ProgressChangedEventArgs( 0, newItemList.Count, newItemList.Count, ProgressChangedEventArgs.ProgressState.Cancelled ) )) );
				}
			} ).ContinueWith( (t) => this.loaderMainTask = null );
		}

		// アイテムリストをcountingItemQueueに設定
		private void SetCountingItemQueue( List<CountableItem> itemList, List<CountableItem> previousItemList = null )
		{
			this.countingItemQueue.Clear();
			foreach( var item in itemList )
			{
				if( previousItemList != null )
				{
					// 再利用の対象はディレクトリ以外
					// ディレクトリは配下のファイルの選択状態が変化してるかもしれないので毎度数え直す
					if( !item.IsDirectory )
					{
						int foundIndex = previousItemList.FindIndex( ( i ) => (i.CombiPath == item.CombiPath) );
						if( foundIndex >= 0 )
						{
							item.SetCountData( previousItemList[foundIndex].CountData );
						}
					}
				}
				this.countingItemQueue.Enqueue( item );
			}
		}

		// キューに積まれたアイテムの計測
		// キャンセル検知時に OperationCanceledException をスロー
		private void CountQueuedItems( int queuedNum, CancellationToken cancelToken )
		{
			while( true )
			{
				cancelToken.ThrowIfCancellationRequested();

				CountableItem item;
				int countedNum;

				lock( this.countingItemQueueLock )
				{
					if( this.countingItemQueue.Count <= 0 )
					{
						// 全部読み切りました
						break;
					}

					item = this.countingItemQueue.Dequeue();
					countedNum = queuedNum - this.countingItemQueue.Count;
				}

				if( !item.IsCounted )
				{
					item.CountItemContent();
				}

				lock( this.countingItemQueueLock )
				{
					// 親ディレクトリの計測結果に加算
					item.AddCountDataToParents( item.CountData );
				}

				if( (countedNum % ItemListView.kSortIntervalOnLoading) == 0 && this.columnSortInfo.Order != SortOrder.None )
				{
					// 並び替え
					ItemListView.SortItems( this.ViewItemList, this.columnSortInfo.Index, this.columnSortInfo.Order == SortOrder.Ascending );
					this.Invoke( (MethodInvoker)(() => this.Refresh()) );
				}

				if( (countedNum % ItemListView.kProgressNotifyInterval) == 0 )
				{
					// 進捗表示更新
					this.Invoke( (MethodInvoker)(() =>
						this.LoadingProgressChanged( this, new ProgressChangedEventArgs( 0, queuedNum, countedNum ) )) );
				}
			}
		}

		// 現在の選択状態をSelectedItemListに反映して通知発行
		private void ReflectSelection( int startIndex, int endIndex, bool isSelected )
		{
			this.SelectedItemList.Clear();
			foreach( int index in this.SelectedIndices )
			{
				this.SelectedItemList.Add( this.ViewItemList[index] );
			}

			this.SelectionChanged( this, new ListViewVirtualItemsSelectionRangeChangedEventArgs( startIndex, endIndex, isSelected ) );
		}

		private static void SortItems( List<CountableItem> items, int columnIndex, bool ascending )
		{
			// ソートキーが同一のものはインデックス順で並べる

			if( columnIndex == -1 )
			{
				// 列指定なしの時はインデックス順
				items.Sort( ( a, b ) => (a.ItemIndex - b.ItemIndex) );
			}
			else if( ItemListView.ColumnParameters[columnIndex].IsCountData )
			{
				// 行数情報の列
				string columnName = ItemListView.ColumnParameters[columnIndex].Name;
				items.Sort( ( a, b ) =>
				{
					//TODO: "-"の考慮
					int aValue = a.CountData[columnName];
					int bValue = b.CountData[columnName];
					if( aValue == bValue )
					{
						return a.ItemIndex - b.ItemIndex;
					}
					return ascending ? (aValue - bValue) : (bValue - aValue);
				} );
			}
			else if( ItemListView.ColumnParameters[columnIndex].IsNumeric )
			{
				// 行数情報以外の数値の列
				items.Sort( ( a, b ) =>
				{
					int aValue = 0;
					int bValue = 0;
					int.TryParse( a.SubItems[columnIndex].Text.Replace( ",", string.Empty ), out aValue );
					int.TryParse( b.SubItems[columnIndex].Text.Replace( ",", string.Empty ), out bValue );
					if( aValue == bValue )
					{
						return a.ItemIndex - b.ItemIndex;
					}
					return ascending ? (aValue - bValue) : (bValue - aValue);
				} );
			}
			else
			{
				// 数値以外の列
				items.Sort( ( a, b ) =>
				{
					string aText = a.SubItems[columnIndex].Text;
					string bText = b.SubItems[columnIndex].Text;
					if( aText == bText )
					{
						return a.ItemIndex - b.ItemIndex;
					}
					return ascending ? string.Compare( aText, bText ) : string.Compare( bText, aText );
				} );
			}
		}

		private void CancelCountItems()
		{
			if( this.cancelTokenSource != null )
			{
				this.cancelTokenSource.Cancel();
			}
		}
	}
}
