﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace ccounter
{
	public struct CountData
	{
		//
		// Public properties
		//

		// Counted properties
		public int AllLines { get; private set; }
		public int BlankLines { get; private set; }
		public int CommentLines { get; private set; }
		public int DisableLines { get; private set; }

		// Calculated properties
		public int EnableLines { get { return this.AllLines - this.DisableLines; } }
		public int EffectiveLines { get { return this.AllLines - this.DisableLines - this.BlankLines - this.CommentLines; } }

		public int[] AdditionalLines { get; private set; }

		public bool SpecificInfoEnabled { get; private set; }

		public int this[string name]
		{
			get
			{
				int count = 0;
				switch( name )
				{
					case "All":
						count = this.AllLines;
						break;
					case "Blank":
						count = this.BlankLines;
						break;
					case "Comment":
						count = this.CommentLines;
						break;
					case "Disable":
						count = this.DisableLines;
						break;
					case "Enable":
						count = this.EnableLines;
						break;
					case "Effective":
						count = this.EffectiveLines;
						break;
					default:
						// 例：Additional999
						//               ^^^ここがインデックス
						if( name.StartsWith( "Additional" ) )
						{
							int index;
							bool result = int.TryParse( name.Substring( "Additional".Length ), out index );
							if( result && 0 <= index && index < this.AdditionalLines.Length )
							{
								count = this.AdditionalLines[index];
							}
						}
						break;
				}

				return count;
			}
		}

		//
		// Public constant fields
		//

		public static readonly string[] kCountedPropertyNames = { "All", "Blank", "Comment", "Disable" };

		//
		// Public methods
		//

		public CountData( int alines, int blines, int clines, int dlines, int[] additionalLines = null )
			: this()
		{
			this.AllLines = alines;
			this.BlankLines = blines;
			this.CommentLines = clines;
			this.DisableLines = dlines;
			this.AdditionalLines = additionalLines;
			this.SpecificInfoEnabled = true;
		}

		public CountData( int alines, int[] additionalLines = null )
			: this()
		{
			this.AllLines = alines;
			this.AdditionalLines = additionalLines;
			this.SpecificInfoEnabled = false;
		}

		public static CountData operator +( CountData left, CountData right )
		{
			var count = new CountData();

			count.AllLines = left.AllLines + right.AllLines;
			count.BlankLines = left.BlankLines + right.BlankLines;
			count.CommentLines = left.CommentLines + right.CommentLines;
			count.DisableLines = left.DisableLines + right.DisableLines;

			if( left.AdditionalLines != null && right.AdditionalLines != null )
			{
				Debug.Assert( left.AdditionalLines.Length == right.AdditionalLines.Length );
				int additional_lines_count = Math.Min( left.AdditionalLines.Length, right.AdditionalLines.Length );
				count.AdditionalLines = new int[additional_lines_count];
				for( int i = 0; i < additional_lines_count; i++ )
				{
					count.AdditionalLines[i] = left.AdditionalLines[i] + right.AdditionalLines[i];
				}
			}

			return count;
		}

		public override bool Equals( object obj )
		{
			return base.Equals( obj );
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		public static bool operator ==( CountData left, CountData right )
		{
			foreach( var name in CountData.kCountedPropertyNames )
			{
				if( left[name] != right[name] )
				{
					return false;
				}
			}

			if( left.AdditionalLines != null && right.AdditionalLines != null )
			{
				if( left.AdditionalLines.Length != right.AdditionalLines.Length )
				{
					return false;
				}
				for( int i = 0; i < left.AdditionalLines.Length; i++ )
				{
					if( left.AdditionalLines[i] != right.AdditionalLines[i] )
					{
						return false;
					}
				}
			}
			else
			{
				if( left.AdditionalLines != right.AdditionalLines )
				{
					return false;
				}
			}

			return true;
		}

		public static bool operator !=( CountData left, CountData right )
		{
			return !(left == right);
		}
	}

	class CodeCounter
	{
		//
		// Public methods
		//

		public static CountData? Analyze( string path )
		{
			int alines = 0;
			int blines = 0;
			int clines = 0;
			int dlines = 0;
			//bool cbInsided = false;

			using( var reader = new StreamReader( path ) )
			{
				string currentLine = string.Empty;
				string nextLine;

				var stateStack = new Stack<State>();
				var ifBlockStack = new Stack<IfBlock>();

				stateStack.Push( State.Normal );
				ifBlockStack.Push( new IfBlock( "", true, false ) );

				try
				{
					while( (nextLine = reader.ReadLine()) != null )
					{
						alines++;

						nextLine = nextLine.Trim();

						// 両端の空白文字を除去
						// '\'による行連結
						if( nextLine.EndsWith( @"\" ) )
						{
							currentLine += nextLine.TrimEnd( '\\' ).TrimEnd();
							continue;
						}
						currentLine += nextLine;

						//currentLine = @"# if 1/* test */";

						// 空白行
						if( currentLine == string.Empty )
						{
							blines++;
							continue;
						}

						if( stateStack.Peek() != State.BlockComment )
						{
							// プリプロセッサによる無効行の判断
							bool skipLine = false;

							var match = Regex.Match( currentLine, @"^\s*#\s*(\w+)(\s+(\w+))?" );
							if( match.Groups[1].Value == "if" )
							{
								if( match.Groups[3].Value == "1" )
								{
									ifBlockStack.Push( new IfBlock( "if", true, false ) );
									skipLine = true;
								}
								else if( match.Groups[3].Value == "0" )
								{
									ifBlockStack.Push( new IfBlock( "if", false, false ) );
									skipLine = true;
								}
								else
								{
									ifBlockStack.Push( new IfBlock( "if", true, true ) );
								}
							}
							//else if( match.Groups[1].Value == "elif" )
							//{
							//	Ignored == true の時はそのまま出力
							//}
							else if(
								match.Groups[1].Value == "ifdef" ||
								match.Groups[1].Value == "ifndef" )
							{
								ifBlockStack.Push( new IfBlock( "if", true, true ) );
							}
							else if( match.Groups[1].Value == "else" )
							{
								var ifBlock = ifBlockStack.Pop();
								if( ifBlock.Section != "if" )
								{
									throw new Exception();
								}
								if( !ifBlock.Ignored )
								{
									ifBlock.Enabled = !ifBlock.Enabled;
									skipLine = true;
								}
								ifBlockStack.Push( ifBlock );
							}
							else if( match.Groups[1].Value == "endif" )
							{
								if( ifBlockStack.Count <= 0 )
								{
									throw new Exception();
								}
								if( !ifBlockStack.Pop().Ignored )
								{
									skipLine = true;
								}
							}
							else
							{
								if( !ifBlockStack.Peek().Enabled )
								{
									skipLine = true;
								}
							}

							if( !skipLine )
							{
								foreach( var ifBlock in ifBlockStack )
								{
									if( !ifBlock.Enabled )
									{
										skipLine = true;
										break;
									}
								}
							}

							if( skipLine )
							{
								dlines++;
								currentLine = string.Empty;
								continue;
							}
						}

						string token = string.Empty;
						bool commentLine = true;

						foreach( char c in currentLine )
						{
							if( stateStack.Peek() == State.EscapeSequence )
							{
								stateStack.Pop();
							}
							else if( stateStack.Peek() == State.BlockComment )
							{
								if( (token + c).EndsWith( "*/" ) )
								{
									// ブロックコメント終了
									stateStack.Pop();
									token = string.Empty;
									continue;
								}
							}
							else if( stateStack.Peek() == State.String )
							{
								if( c == '"' )
								{
									// 文字列終了
									stateStack.Pop();
									token = string.Empty;
									continue;
								}
							}
							else if( stateStack.Peek() == State.Slash )
							{
								stateStack.Pop();

								if( c == '/' )
								{
									token = string.Empty;
									break;
								}
								else if( c == '*' )
								{
									token = string.Empty;
									stateStack.Push( State.BlockComment );
								}
							}

							if( stateStack.Peek() == State.Normal )
							{
								if( c == '/' )
								{
									// コメント開始かも
									stateStack.Push( State.Slash );
								}
								else if( c == '\\' )
								{
									stateStack.Push( State.EscapeSequence );
								}
								else if( c == '"' )
								{
									token = string.Empty;
									stateStack.Push( State.String );
								}
							}

							if( stateStack.Peek() != State.BlockComment &&
								stateStack.Peek() != State.Slash )
							{
								commentLine = false;
							}

							token += c;
						}

						if( commentLine )
						{
							// コメントだけの行
							clines++;
						}

						currentLine = string.Empty;
					}
				}
				catch( Exception ex )
				{
					Util.WriteLog( ex );
					return null;
				}
			}

			return new CountData( alines, blines, clines, dlines );
		}

		//
		// Private methods
		//

		private CodeCounter()
		{
		}

		private enum State
		{
			Normal,
			String,
			Slash,
			BlockComment,
			EscapeSequence,
		}

		private struct IfBlock
		{
			public IfBlock( string section, bool enabled, bool ignored )
			{
				this.Section = section;
				this.Enabled = enabled;
				this.Ignored = ignored;
			}
			public string Section;
			public bool Enabled;
			public bool Ignored;
		}
	}
}
