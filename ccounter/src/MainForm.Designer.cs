﻿namespace ccounter
{
	partial class MainForm
	{
		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		/// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && (components != null) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows フォーム デザイナーで生成されたコード

		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabel_countInfo = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel_info = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel_progress = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.toolStripContainer_tree = new System.Windows.Forms.ToolStripContainer();
			this.toolStrip2 = new System.Windows.Forms.ToolStrip();
			this.toolStripButton_checkAllDirectories = new System.Windows.Forms.ToolStripButton();
			this.toolStripButton_clearAllDirectories = new System.Windows.Forms.ToolStripButton();
			this.splitContainer2 = new System.Windows.Forms.SplitContainer();
			this.toolStripContainer_itemList = new System.Windows.Forms.ToolStripContainer();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.toolStripButton_selectAllFile = new System.Windows.Forms.ToolStripButton();
			this.toolStripButton_copyFiles = new System.Windows.Forms.ToolStripButton();
			this.toolStripButton_refleshFiles = new System.Windows.Forms.ToolStripButton();
			this.toolStripContainer_funcList = new System.Windows.Forms.ToolStripContainer();
			this.listView2 = new System.Windows.Forms.ListView();
			this.toolStrip3 = new System.Windows.Forms.ToolStrip();
			this.toolStripButton_selectAllFunc = new System.Windows.Forms.ToolStripButton();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.toolStripMenuItem_about = new System.Windows.Forms.ToolStripMenuItem();
			this.imageList_general = new System.Windows.Forms.ImageList(this.components);
			this.imageList_itemListView = new System.Windows.Forms.ImageList(this.components);
			this.imageList_dirTreeView = new System.Windows.Forms.ImageList(this.components);
			this.directoryTreeView = new ccounter.DirectoryTreeView();
			this.itemListView = new ccounter.ItemListView();
			this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
			this.toolStripContainer1.ContentPanel.SuspendLayout();
			this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
			this.toolStripContainer1.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.toolStripContainer_tree.ContentPanel.SuspendLayout();
			this.toolStripContainer_tree.TopToolStripPanel.SuspendLayout();
			this.toolStripContainer_tree.SuspendLayout();
			this.toolStrip2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
			this.splitContainer2.Panel1.SuspendLayout();
			this.splitContainer2.Panel2.SuspendLayout();
			this.splitContainer2.SuspendLayout();
			this.toolStripContainer_itemList.ContentPanel.SuspendLayout();
			this.toolStripContainer_itemList.TopToolStripPanel.SuspendLayout();
			this.toolStripContainer_itemList.SuspendLayout();
			this.toolStrip1.SuspendLayout();
			this.toolStripContainer_funcList.ContentPanel.SuspendLayout();
			this.toolStripContainer_funcList.TopToolStripPanel.SuspendLayout();
			this.toolStripContainer_funcList.SuspendLayout();
			this.toolStrip3.SuspendLayout();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// toolStripContainer1
			// 
			// 
			// toolStripContainer1.BottomToolStripPanel
			// 
			this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
			// 
			// toolStripContainer1.ContentPanel
			// 
			this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
			this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(796, 341);
			this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
			this.toolStripContainer1.Name = "toolStripContainer1";
			this.toolStripContainer1.Size = new System.Drawing.Size(796, 388);
			this.toolStripContainer1.TabIndex = 0;
			this.toolStripContainer1.Text = "toolStripContainer1";
			// 
			// toolStripContainer1.TopToolStripPanel
			// 
			this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.menuStrip1);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel_countInfo,
            this.toolStripStatusLabel_info,
            this.toolStripStatusLabel_progress,
            this.toolStripProgressBar1});
			this.statusStrip1.Location = new System.Drawing.Point(0, 0);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(796, 23);
			this.statusStrip1.TabIndex = 0;
			// 
			// toolStripStatusLabel_countInfo
			// 
			this.toolStripStatusLabel_countInfo.Name = "toolStripStatusLabel_countInfo";
			this.toolStripStatusLabel_countInfo.Size = new System.Drawing.Size(38, 18);
			this.toolStripStatusLabel_countInfo.Text = "count";
			// 
			// toolStripStatusLabel_info
			// 
			this.toolStripStatusLabel_info.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.toolStripStatusLabel_info.Name = "toolStripStatusLabel_info";
			this.toolStripStatusLabel_info.Size = new System.Drawing.Size(28, 18);
			this.toolStripStatusLabel_info.Text = "info";
			this.toolStripStatusLabel_info.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// toolStripStatusLabel_progress
			// 
			this.toolStripStatusLabel_progress.Name = "toolStripStatusLabel_progress";
			this.toolStripStatusLabel_progress.Size = new System.Drawing.Size(52, 18);
			this.toolStripStatusLabel_progress.Text = "progress";
			// 
			// toolStripProgressBar1
			// 
			this.toolStripProgressBar1.Name = "toolStripProgressBar1";
			this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 17);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.toolStripContainer_tree);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
			this.splitContainer1.Size = new System.Drawing.Size(796, 341);
			this.splitContainer1.SplitterDistance = 307;
			this.splitContainer1.TabIndex = 0;
			// 
			// toolStripContainer_tree
			// 
			// 
			// toolStripContainer_tree.ContentPanel
			// 
			this.toolStripContainer_tree.ContentPanel.Controls.Add(this.directoryTreeView);
			this.toolStripContainer_tree.ContentPanel.Size = new System.Drawing.Size(244, 188);
			this.toolStripContainer_tree.Location = new System.Drawing.Point(12, 18);
			this.toolStripContainer_tree.Name = "toolStripContainer_tree";
			this.toolStripContainer_tree.Size = new System.Drawing.Size(244, 213);
			this.toolStripContainer_tree.TabIndex = 1;
			this.toolStripContainer_tree.Text = "toolStripContainer2";
			// 
			// toolStripContainer_tree.TopToolStripPanel
			// 
			this.toolStripContainer_tree.TopToolStripPanel.Controls.Add(this.toolStrip2);
			// 
			// toolStrip2
			// 
			this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
			this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_checkAllDirectories,
            this.toolStripButton_clearAllDirectories});
			this.toolStrip2.Location = new System.Drawing.Point(3, 0);
			this.toolStrip2.Name = "toolStrip2";
			this.toolStrip2.Size = new System.Drawing.Size(109, 25);
			this.toolStrip2.TabIndex = 0;
			// 
			// toolStripButton_checkAllDirectories
			// 
			this.toolStripButton_checkAllDirectories.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton_checkAllDirectories.Name = "toolStripButton_checkAllDirectories";
			this.toolStripButton_checkAllDirectories.Size = new System.Drawing.Size(60, 22);
			this.toolStripButton_checkAllDirectories.Text = "Check All";
			this.toolStripButton_checkAllDirectories.Click += new System.EventHandler(this.toolStripButton_checkAllDirectories_Click);
			// 
			// toolStripButton_clearAllDirectories
			// 
			this.toolStripButton_clearAllDirectories.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton_clearAllDirectories.Name = "toolStripButton_clearAllDirectories";
			this.toolStripButton_clearAllDirectories.Size = new System.Drawing.Size(37, 22);
			this.toolStripButton_clearAllDirectories.Text = "Clear";
			this.toolStripButton_clearAllDirectories.Click += new System.EventHandler(this.toolStripButton_clearDirectory_Click);
			// 
			// splitContainer2
			// 
			this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer2.Location = new System.Drawing.Point(0, 0);
			this.splitContainer2.Name = "splitContainer2";
			this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer2.Panel1
			// 
			this.splitContainer2.Panel1.Controls.Add(this.toolStripContainer_itemList);
			// 
			// splitContainer2.Panel2
			// 
			this.splitContainer2.Panel2.Controls.Add(this.toolStripContainer_funcList);
			this.splitContainer2.Size = new System.Drawing.Size(485, 341);
			this.splitContainer2.SplitterDistance = 160;
			this.splitContainer2.TabIndex = 1;
			// 
			// toolStripContainer_itemList
			// 
			// 
			// toolStripContainer_itemList.ContentPanel
			// 
			this.toolStripContainer_itemList.ContentPanel.Controls.Add(this.itemListView);
			this.toolStripContainer_itemList.ContentPanel.Size = new System.Drawing.Size(350, 109);
			this.toolStripContainer_itemList.Location = new System.Drawing.Point(37, 18);
			this.toolStripContainer_itemList.Name = "toolStripContainer_fileList";
			this.toolStripContainer_itemList.Size = new System.Drawing.Size(350, 134);
			this.toolStripContainer_itemList.TabIndex = 1;
			this.toolStripContainer_itemList.Text = "toolStripContainer3";
			// 
			// toolStripContainer_itemList.TopToolStripPanel
			// 
			this.toolStripContainer_itemList.TopToolStripPanel.Controls.Add(this.toolStrip1);
			// 
			// toolStrip1
			// 
			this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_selectAllFile,
            this.toolStripButton_copyFiles,
            this.toolStripButton_refleshFiles});
			this.toolStrip1.Location = new System.Drawing.Point(3, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(207, 25);
			this.toolStrip1.TabIndex = 0;
			// 
			// toolStripButton_selectAllFile
			// 
			this.toolStripButton_selectAllFile.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_selectAllFile.Image")));
			this.toolStripButton_selectAllFile.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton_selectAllFile.Name = "toolStripButton_selectAllFile";
			this.toolStripButton_selectAllFile.Size = new System.Drawing.Size(75, 22);
			this.toolStripButton_selectAllFile.Text = "Select All";
			this.toolStripButton_selectAllFile.ToolTipText = "Select All (Ctrl + A)\r\n";
			this.toolStripButton_selectAllFile.Click += new System.EventHandler(this.toolStripButton_selectAllFile_Click);
			// 
			// toolStripButton_copyFiles
			// 
			this.toolStripButton_copyFiles.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_copyFiles.Image")));
			this.toolStripButton_copyFiles.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton_copyFiles.Name = "toolStripButton_copyFiles";
			this.toolStripButton_copyFiles.Size = new System.Drawing.Size(54, 22);
			this.toolStripButton_copyFiles.Text = "Copy";
			this.toolStripButton_copyFiles.ToolTipText = "Copy (Ctrl + C)";
			this.toolStripButton_copyFiles.Click += new System.EventHandler(this.toolStripButton_copyFiles_Click);
			// 
			// toolStripButton_refleshFiles
			// 
			this.toolStripButton_refleshFiles.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_refleshFiles.Image")));
			this.toolStripButton_refleshFiles.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton_refleshFiles.Name = "toolStripButton_refleshFiles";
			this.toolStripButton_refleshFiles.Size = new System.Drawing.Size(66, 22);
			this.toolStripButton_refleshFiles.Text = "Refresh";
			this.toolStripButton_refleshFiles.ToolTipText = "Refresh (F5)";
			this.toolStripButton_refleshFiles.Click += new System.EventHandler(this.toolStripButton_refleshFiles_Click);
			// 
			// toolStripContainer_funcList
			// 
			this.toolStripContainer_funcList.BottomToolStripPanelVisible = false;
			// 
			// toolStripContainer_funcList.ContentPanel
			// 
			this.toolStripContainer_funcList.ContentPanel.Controls.Add(this.listView2);
			this.toolStripContainer_funcList.ContentPanel.Size = new System.Drawing.Size(225, 84);
			this.toolStripContainer_funcList.LeftToolStripPanelVisible = false;
			this.toolStripContainer_funcList.Location = new System.Drawing.Point(53, 26);
			this.toolStripContainer_funcList.Name = "toolStripContainer_funcList";
			this.toolStripContainer_funcList.RightToolStripPanelVisible = false;
			this.toolStripContainer_funcList.Size = new System.Drawing.Size(225, 109);
			this.toolStripContainer_funcList.TabIndex = 1;
			this.toolStripContainer_funcList.Text = "toolStripContainer2";
			// 
			// toolStripContainer_funcList.TopToolStripPanel
			// 
			this.toolStripContainer_funcList.TopToolStripPanel.Controls.Add(this.toolStrip3);
			// 
			// listView2
			// 
			this.listView2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listView2.Location = new System.Drawing.Point(0, 0);
			this.listView2.Name = "listView2";
			this.listView2.Size = new System.Drawing.Size(225, 84);
			this.listView2.TabIndex = 0;
			this.listView2.UseCompatibleStateImageBehavior = false;
			this.listView2.View = System.Windows.Forms.View.Details;
			// 
			// toolStrip3
			// 
			this.toolStrip3.Dock = System.Windows.Forms.DockStyle.None;
			this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_selectAllFunc});
			this.toolStrip3.Location = new System.Drawing.Point(3, 0);
			this.toolStrip3.Name = "toolStrip3";
			this.toolStrip3.Size = new System.Drawing.Size(87, 25);
			this.toolStrip3.TabIndex = 0;
			// 
			// toolStripButton_selectAllFunc
			// 
			this.toolStripButton_selectAllFunc.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_selectAllFunc.Image")));
			this.toolStripButton_selectAllFunc.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton_selectAllFunc.Name = "toolStripButton_selectAllFunc";
			this.toolStripButton_selectAllFunc.Size = new System.Drawing.Size(75, 22);
			this.toolStripButton_selectAllFunc.Text = "Select All";
			// 
			// menuStrip1
			// 
			this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_about});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(796, 24);
			this.menuStrip1.TabIndex = 1;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// toolStripMenuItem_about
			// 
			this.toolStripMenuItem_about.Name = "toolStripMenuItem_about";
			this.toolStripMenuItem_about.Size = new System.Drawing.Size(52, 20);
			this.toolStripMenuItem_about.Text = "About";
			this.toolStripMenuItem_about.Click += new System.EventHandler(this.toolStripMenuItem_about_Click);
			// 
			// imageList_general
			// 
			this.imageList_general.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_general.ImageStream")));
			this.imageList_general.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList_general.Images.SetKeyName(0, "cross.png");
			this.imageList_general.Images.SetKeyName(1, "folder.png");
			this.imageList_general.Images.SetKeyName(2, "page_copy.png");
			this.imageList_general.Images.SetKeyName(3, "wand.png");
			this.imageList_general.Images.SetKeyName(4, "cursor.png");
			this.imageList_general.Images.SetKeyName(5, "arrow_refresh.png");
			this.imageList_general.Images.SetKeyName(6, "information.png");
			this.imageList_general.Images.SetKeyName(7, "calculator.png");
			this.imageList_general.Images.SetKeyName(8, "folder.png");
			this.imageList_general.Images.SetKeyName(9, "folder_delete.png");
			this.imageList_general.Images.SetKeyName(10, "folder_page.png");
			this.imageList_general.Images.SetKeyName(11, "tick.png");
			// 
			// imageList_itemListView
			// 
			this.imageList_itemListView.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_fileListView.ImageStream")));
			this.imageList_itemListView.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList_itemListView.Images.SetKeyName(0, "page_white_c.png");
			this.imageList_itemListView.Images.SetKeyName(1, "page_white_cplusplus.png");
			this.imageList_itemListView.Images.SetKeyName(2, "page_white_h.png");
			this.imageList_itemListView.Images.SetKeyName(3, "page_white_text.png");
			this.imageList_itemListView.Images.SetKeyName(4, "bullet_arrow_up.png");
			this.imageList_itemListView.Images.SetKeyName(5, "bullet_arrow_down.png");
			this.imageList_itemListView.Images.SetKeyName(6, "folder.png");
			// 
			// imageList_dirTreeView
			// 
			this.imageList_dirTreeView.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_dirTreeView.ImageStream")));
			this.imageList_dirTreeView.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList_dirTreeView.Images.SetKeyName(0, "folder.png");
			// 
			// directoryTreeView
			// 
			this.directoryTreeView.CheckBoxes = true;
			this.directoryTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.directoryTreeView.Location = new System.Drawing.Point(0, 0);
			this.directoryTreeView.Name = "directoryTreeView";
			this.directoryTreeView.Size = new System.Drawing.Size(244, 188);
			this.directoryTreeView.TabIndex = 0;
			// 
			// itemListView
			// 
			this.itemListView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.itemListView.FileNameFilter = null;
			this.itemListView.FullRowSelect = true;
			this.itemListView.GridLines = true;
			this.itemListView.HideSelection = false;
			this.itemListView.Location = new System.Drawing.Point(0, 0);
			this.itemListView.Name = "fileListView";
			this.itemListView.Size = new System.Drawing.Size(350, 109);
			this.itemListView.TabIndex = 0;
			this.itemListView.UseCompatibleStateImageBehavior = false;
			this.itemListView.View = System.Windows.Forms.View.Details;
			this.itemListView.VirtualMode = true;
			// 
			// MainForm
			// 
			this.AllowDrop = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(796, 388);
			this.Controls.Add(this.toolStripContainer1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "MainForm";
			this.Text = "ccounter";
			this.Load += new System.EventHandler(this.FormMain_Load);
			this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
			this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
			this.toolStripContainer1.ContentPanel.ResumeLayout(false);
			this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
			this.toolStripContainer1.TopToolStripPanel.PerformLayout();
			this.toolStripContainer1.ResumeLayout(false);
			this.toolStripContainer1.PerformLayout();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.toolStripContainer_tree.ContentPanel.ResumeLayout(false);
			this.toolStripContainer_tree.TopToolStripPanel.ResumeLayout(false);
			this.toolStripContainer_tree.TopToolStripPanel.PerformLayout();
			this.toolStripContainer_tree.ResumeLayout(false);
			this.toolStripContainer_tree.PerformLayout();
			this.toolStrip2.ResumeLayout(false);
			this.toolStrip2.PerformLayout();
			this.splitContainer2.Panel1.ResumeLayout(false);
			this.splitContainer2.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
			this.splitContainer2.ResumeLayout(false);
			this.toolStripContainer_itemList.ContentPanel.ResumeLayout(false);
			this.toolStripContainer_itemList.TopToolStripPanel.ResumeLayout(false);
			this.toolStripContainer_itemList.TopToolStripPanel.PerformLayout();
			this.toolStripContainer_itemList.ResumeLayout(false);
			this.toolStripContainer_itemList.PerformLayout();
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.toolStripContainer_funcList.ContentPanel.ResumeLayout(false);
			this.toolStripContainer_funcList.TopToolStripPanel.ResumeLayout(false);
			this.toolStripContainer_funcList.TopToolStripPanel.PerformLayout();
			this.toolStripContainer_funcList.ResumeLayout(false);
			this.toolStripContainer_funcList.PerformLayout();
			this.toolStrip3.ResumeLayout(false);
			this.toolStrip3.PerformLayout();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ToolStripContainer toolStripContainer1;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_about;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_info;
		private System.Windows.Forms.SplitContainer splitContainer2;
		private System.Windows.Forms.ToolStripContainer toolStripContainer_tree;
		private System.Windows.Forms.ToolStrip toolStrip2;
		private System.Windows.Forms.ToolStripButton toolStripButton_clearAllDirectories;
		private System.Windows.Forms.ToolStripContainer toolStripContainer_itemList;
		private System.Windows.Forms.ToolStripContainer toolStripContainer_funcList;
		private System.Windows.Forms.ToolStrip toolStrip3;
		private System.Windows.Forms.ToolStripButton toolStripButton_selectAllFunc;
		private DirectoryTreeView directoryTreeView;
		private ItemListView itemListView;
		private System.Windows.Forms.ListView listView2;
		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.ToolStripButton toolStripButton_selectAllFile;
		private System.Windows.Forms.ToolStripButton toolStripButton_copyFiles;
		private System.Windows.Forms.ImageList imageList_general;
		private System.Windows.Forms.ImageList imageList_itemListView;
		private System.Windows.Forms.ToolStripButton toolStripButton_refleshFiles;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_countInfo;
		private System.Windows.Forms.ImageList imageList_dirTreeView;
		private System.Windows.Forms.ToolStripButton toolStripButton_checkAllDirectories;
		private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_progress;
	}
}

