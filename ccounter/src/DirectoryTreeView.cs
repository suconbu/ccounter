﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace ccounter
{
	class DirectoryTreeView : TreeView
	{
		//
		// Public properties
		//

		public event EventHandler CheckedNodeChanged = delegate { };
		public event EventHandler SelectedNodeChanged = delegate { };
		public event EventHandler DirectoryNumChanged = delegate { };
		public event TreeNodeMouseClickEventHandler NodeDoubleClicked = delegate { };
		public int DirectoryNum { get; private set; }
		public string[] CheckedDirectoryPaths { get; private set; }
		public string SelectedDirectoryPath { get { return ((DirectoryTreeNode)this.SelectedNode).DirectoryCombiPath; } }

		//
		// Public methods
		//

		public DirectoryTreeView()
		{
			this.CheckBoxes = true;
			this.CheckedDirectoryPaths = new string[] { };
			this.AfterSelect += DirectoryTreeView_AfterSelect;
			this.AfterCheck += DirectoryTreeView_AfterCheck;
			this.NodeMouseClick += DirectoryTreeView_NodeMouseClick;
		}

		public DirectoryTreeNode FindNode( string combiPath )
		{
			string rootPath = DirectoryTreeNode.GetRootPath( combiPath );
			string treePath = DirectoryTreeNode.GetTreePath( combiPath );
			string[] tokens = treePath.Split( new string[] { this.PathSeparator }, StringSplitOptions.RemoveEmptyEntries );
			var nextNodes = this.Nodes;
			DirectoryTreeNode node = null;
			foreach( string nodeName in tokens )
			{
				node = (DirectoryTreeNode)nextNodes[nodeName];
				if( node == null ||
					DirectoryTreeNode.GetRootPath( node.DirectoryCombiPath ) != rootPath )
				{
					return null;
				}
				nextNodes = node.Nodes;
			}
			return (DirectoryTreeNode)node;
		}

		public void AddDirectories( string[] paths )
		{
			if( paths == null )
			{
				return;
			}

			this.BeginUpdate();

			var addPaths = new List<string>();
			DirectoryTreeNode firstNode = null;
			try
			{
				foreach( var path in paths )
				{
					string addPath = Path.GetFullPath( path ).TrimEnd( '\\' );
					if( Directory.Exists( addPath ) )
					{
						if( !this.rootDirectoryPaths.Exists( ( a ) => (a == addPath) ) )
						{
							var node = DirectoryTreeNode.Create( addPath );
							node.ExpandAll();
							this.Nodes.Add( node );
							if( firstNode == null )
							{
								firstNode = node;
							}
							this.rootDirectoryPaths.Add( addPath );
						}
					}
				}
			}
			catch( Exception ex )
			{
				Console.Write( ex.ToString() );
			}

			this.EndUpdate();

			this.DirectoryNum = this.GetNodeCount( true );
			if( this.DirectoryNum > 0 )
			{
				this.Focus();
				this.SelectedNode = firstNode;
			}

			this.DirectoryNumChanged( this, new EventArgs() );
		}

		public void ClearDirectories()
		{
			this.Nodes.Clear();
			this.rootDirectoryPaths.Clear();
			this.DirectoryNum = 0;
			this.DirectoryNumChanged( this, new EventArgs() );

			this.UpdateCheckedNodes();
			this.CheckedNodeChanged( this, new EventArgs() );
		}

		public void SetCheckAll( bool check )
		{
			foreach( DirectoryTreeNode node in this.Nodes )
			{
				this.checkEventBlocking = true;
				node.Checked = check;
				node.SetChildNodeChecks( check );
				this.checkEventBlocking = false;
			}

			this.UpdateCheckedNodes();
			this.CheckedNodeChanged( this, new EventArgs() );
		}

		// 指定したパスを持つディレクトリノードを強調表示
		// 同時に強調表示できるノードは1つのみ
		// dirPathがnullの場合は現在の強調表示を解除
		public void HighlightNode( string combiPath )
		{
			// まず現在の強調表示を削除
			if( this.highlightedNode != null )
			{
				this.highlightedNode.State = this.highlightedNode.Checked ? DirectoryTreeNode.NodeState.Checked : DirectoryTreeNode.NodeState.Unchecked;
			}

			if( combiPath != null )
			{
				var node = this.FindNode( combiPath );
				if( node != this.highlightedNode )
				{
					// 現在とは別のノードが強調表示に
					// 画面外だったら頭出ししてあげる
					node.State = DirectoryTreeNode.NodeState.Highlighted;
					if( !node.IsVisible )
					{
						this.TopNode = node;
					}
					this.highlightedNode = node;
				}
			}
			else
			{
				this.highlightedNode = null;
			}
		}

		//
		// Private fields
		//

		private List<string> rootDirectoryPaths = new List<string>();
		private bool checkEventBlocking = false;
		private DirectoryTreeNode highlightedNode = null;

		//
		// Private methods
		//

		void DirectoryTreeView_NodeMouseClick( object sender, TreeNodeMouseClickEventArgs e )
		{
			this.NodeDoubleClicked( sender, e );
		}

		void DirectoryTreeView_AfterSelect( object sender, TreeViewEventArgs e )
		{
			this.SelectedNodeChanged( sender, new EventArgs() );
		}

		void DirectoryTreeView_AfterCheck( object sender, TreeViewEventArgs e )
		{
			if( !this.checkEventBlocking )
			{
				this.checkEventBlocking = true;
				((DirectoryTreeNode)e.Node).SetChildNodeChecks( e.Node.Checked );
				this.checkEventBlocking = false;

				this.UpdateCheckedNodes();
				this.CheckedNodeChanged( sender, e );
			}
		}

		private void UpdateCheckedNodes()
		{
			var checkedNodes = new List<DirectoryTreeNode>();

			foreach( DirectoryTreeNode node in this.Nodes )
			{
				node.GetCheckedNodes( ref checkedNodes );
			}

			var paths = new List<string>();
			foreach( var node in checkedNodes )
			{
				string path = node.DirectoryCombiPath;
				paths.Add( path );
			}

			this.CheckedDirectoryPaths = paths.ToArray();
		}
	}

	class DirectoryTreeNode : TreeNode
	{
		//
		// Public properties
		//

		public string DirectoryCombiPath
		{
			get
			{
				// まず自身のルートノードを得る
				TreeNode rootNode = this;
				while( rootNode.Parent != null )
				{
					rootNode = rootNode.Parent;
				}
				// ベースノードのtagはベースパス文字列
				// ".."はツリー相対パス開始の目印
				return ((NodeTag)rootNode.Tag).DirectoryPath + @"\..\" + this.FullPath;
			}
		}

		public enum NodeState
		{
			Highlighted,
			Checked,
			Unchecked,
		}

		public NodeState State
		{
			set
			{
				switch( value )
				{
					case NodeState.Highlighted:
						this.ForeColor = SystemColors.WindowText;
						this.BackColor = Color.DeepPink;
						break;
					case NodeState.Checked:
						this.ForeColor = SystemColors.WindowText;
						this.BackColor = Color.LightSkyBlue;
						break;
					case NodeState.Unchecked:
						this.ForeColor = SystemColors.WindowText;
						this.BackColor = SystemColors.Window;
						break;
				}
			}
		}

		//
		// Public methods
		//

		public static DirectoryTreeNode Create( string directoryPath, bool recursive = true )
		{
			var node = new DirectoryTreeNode();

			node.SetDirectoryRecursive( directoryPath, recursive );
			node.Tag = new NodeTag( directoryPath );

			return node;
		}

		// ツリー内相対パス
		// コンビパスが「C:\aleft\bright\ccount\..\ccount\dd\ee\gg.h」なら「cc\dd\ee\gg.h」の部分
		public static string GetTreePath( string combiPath )
		{
			int index = combiPath.IndexOf( ".." );
			if( index < 0 || combiPath.Length <= (index + 3) )
			{
				return string.Empty;
			}
			return combiPath.Substring( index + 3 );
		}

		// ルートフォルダの絶対パス
		// コンビパスが「C:\aleft\bright\ccount\..\ccount\dd\ee\gg.h」なら「C:\aleft\bright\ccount」の部分
		public static string GetRootPath( string combiPath )
		{
			int index = combiPath.IndexOf( ".." );
			if( index < 1 )
			{
				return string.Empty;
			}
			return combiPath.Substring( 0, index - 1 );
		}

		public void GetCheckedNodes( ref List<DirectoryTreeNode> checkedNodes, bool recursive = true )
		{
			this.GetCheckedNodesRecursive( ref checkedNodes, recursive );
		}

		public void SetChildNodeChecks( bool check )
		{
			this.SetChildNodeChecksRecursive( check );
			//this.UpdateCheckedNodes();
		}

		//
		// Private methods
		//

		private DirectoryTreeNode()
		{
		}

		// これはルートノードのTagにだけ設定されています
		private struct NodeTag
		{
			public NodeTag( string directoryPath )
				: this()
			{
				this.DirectoryPath = directoryPath;
			}
			public string DirectoryPath { get; private set; }
		}

		private void GetCheckedNodesRecursive( ref List<DirectoryTreeNode> checkedNodes, bool recursive = true )
		{
			if( this.Checked )
			{
				checkedNodes.Add( this );
			}

			if( recursive )
			{
				foreach( DirectoryTreeNode node in this.Nodes )
				{
					node.GetCheckedNodesRecursive( ref checkedNodes, recursive );
				}
			}
		}

		private void SetChildNodeChecksRecursive( bool check )
		{
			var state = check ? DirectoryTreeNode.NodeState.Checked : DirectoryTreeNode.NodeState.Unchecked;
			this.State = state;
			foreach( DirectoryTreeNode childNode in this.Nodes )
			{
				childNode.State = state;
				childNode.Checked = check;
				childNode.SetChildNodeChecksRecursive( check );
			}
		}

		private void SetDirectoryRecursive( string directoryPath, bool recursive = true )
		{
			string name = Path.GetFileName( directoryPath );

			this.Name = name;
			this.Text = name;
			this.ImageKey = "folder.png";

			if( recursive )
			{
				foreach( var path in Util.GetDirectoriesInDirectory( directoryPath, ".*", @"\..*" ) )
				{
					var node = new DirectoryTreeNode();
					node.SetDirectoryRecursive( path );
					this.Nodes.Add( node );
				}
			}
		}
	}
}
