﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ccounter
{
	public partial class MainForm : Form
	{
		//
		// Public properties
		//

		public string[] InitialPaths { get; set; }

		//
		// Public methods
		//

		public MainForm()
		{
			InitializeComponent();
		}

		public static string CountDataToText( CountData countData, string[] propertyNames )
		{
			var text = new StringBuilder();
			foreach( string name in propertyNames )
			{
				text.Append( countData[name] + "\t" );
			}
			// 末尾のタブ文字を削る
			text.Remove( text.Length - 1, 1 );

			return text.ToString();
		}

		//
		// Private fields
		//

		private ContextMenuStrip dirContextMenu = new ContextMenuStrip();
		private ContextMenuStrip fileContextMenu = new ContextMenuStrip();
		private Timer notifyMessageTimer = new Timer();
		private Timer progressBarTimer = new Timer();
		private string statusInfoTextBackup;
		private DateTime workingStartedTime;

		//
		// Private methods
		//

		private void AddDirectory( string[] paths )
		{
			this.directoryTreeView.AddDirectories( paths );
		}

		private void FormMain_Load( object sender, EventArgs e )
		{
			Util.TraverseControls( this, ( c ) => c.Font = SystemFonts.MessageBoxFont );

			this.DragEnter += FormMain_DragEnter;
			this.DragDrop += FormMain_DragDrop;
			this.notifyMessageTimer.Tick += notifyMessageTimer_Tick;
			this.progressBarTimer.Interval = 3000;
			this.progressBarTimer.Tick += progressBarTimer_Tick;

			this.toolStripContainer_tree.Dock = DockStyle.Fill;
			this.toolStripContainer_itemList.Dock = DockStyle.Fill;
			this.toolStripContainer_funcList.Dock = DockStyle.Fill;

			this.splitContainer1.SplitterDistance = 300;
			this.splitContainer1.SplitterIncrement = 10;
			this.splitContainer1.FixedPanel = FixedPanel.Panel1;
			this.splitContainer2.Panel2Collapsed = true;

			string fileNameFilter = @".*\.(c|cpp|cxx|h|hpp|hxx)$";

			// ディレクトリツリーの準備
			this.directoryTreeView.ImageList = this.imageList_dirTreeView;
			this.directoryTreeView.DirectoryNumChanged += directoryTreeView_DirectoryNumChanged;
			this.directoryTreeView.NodeMouseDoubleClick += directoryTreeView_NodeMouseDoubleClick;
			this.directoryTreeView.CheckedNodeChanged += directoryTreeView_CheckedNodeChanged;
			this.directoryTreeView.MouseClick += directoryTreeView_MouseClick;
			this.directoryTreeView.KeyDown += directoryTreeView_KeyDown;
			this.directoryTreeView.NodeDoubleClicked += directoryTreeView_NodeClicked;

			// ファイルリストの準備
			this.itemListView.FileNameFilter = fileNameFilter;
			this.itemListView.SmallImageList = this.imageList_itemListView;
			this.itemListView.SelectionChanged += itemListView_SelectionChanged;
			this.itemListView.KeyDown += itemListView_KeyDown;
			this.itemListView.MouseClick += itemListView_MouseClick;
			this.itemListView.MouseDoubleClick += itemListView_MouseDoubleClick;
			this.itemListView.LoadingProgressChanged += itemListView_ProgressChanged;
			this.itemListView.Scrollable = true;

			// ツールバー
			this.toolStrip1.GripStyle = ToolStripGripStyle.Hidden;
			this.toolStrip2.GripStyle = ToolStripGripStyle.Hidden;
			this.toolStrip3.GripStyle = ToolStripGripStyle.Hidden;
			this.toolStripButton_checkAllDirectories.Image = this.imageList_general.Images["tick.png"];
			this.toolStripButton_clearAllDirectories.Image = this.imageList_general.Images["folder_delete.png"];
			this.toolStripButton_selectAllFile.Image = this.imageList_general.Images["wand.png"];
			this.toolStripButton_copyFiles.Image = this.imageList_general.Images["page_copy.png"];
			this.toolStripButton_refleshFiles.Image = this.imageList_general.Images["arrow_refresh.png"];

			// ステータスバー
			this.toolStripStatusLabel_countInfo.DisplayStyle = ToolStripItemDisplayStyle.None;
			this.toolStripStatusLabel_countInfo.Text = string.Empty;
			this.toolStripStatusLabel_countInfo.Alignment = ToolStripItemAlignment.Left;
			this.toolStripStatusLabel_countInfo.TextAlign = ContentAlignment.MiddleLeft;
			this.toolStripStatusLabel_countInfo.ImageAlign = ContentAlignment.MiddleLeft;
			this.toolStripStatusLabel_countInfo.Image = this.imageList_general.Images["calculator.png"];
			this.toolStripStatusLabel_countInfo.Spring = true;
			this.toolStripStatusLabel_info.Text = string.Empty;
			this.toolStripStatusLabel_info.Alignment = ToolStripItemAlignment.Right;
			this.toolStripStatusLabel_info.TextAlign = ContentAlignment.MiddleLeft;
			this.toolStripStatusLabel_info.ImageAlign = ContentAlignment.MiddleLeft;
			//this.toolStripStatusLabel_info.Spring = true;
			this.toolStripStatusLabel_progress.Visible = false;
			this.toolStripStatusLabel_progress.Text = string.Empty;

			// プログレスバー
			this.toolStripProgressBar1.Visible = false;
			this.toolStripProgressBar1.Style = ProgressBarStyle.Continuous;

			// コンテキストメニューの準備
			this.dirContextMenu.Items.Add( new ToolStripMenuItem(
				"Open with explorer",
				this.imageList_general.Images["folder.png"],
				( eventSender, eventArgs ) => System.Diagnostics.Process.Start( this.directoryTreeView.SelectedDirectoryPath )
				) );
			this.dirContextMenu.Items.Add( new ToolStripSeparator() );
			this.dirContextMenu.Items.Add( new ToolStripMenuItem(
				"Collapse this level",
				null,
				( eventSender, eventArgs ) =>
				{
					var selectedNode = this.directoryTreeView.SelectedNode;
					TreeNodeCollection nodes = null;
					if( selectedNode.Parent != null )
					{
						nodes = selectedNode.Parent.Nodes;
					}
					else
					{
						nodes = this.directoryTreeView.Nodes;
					}
					foreach( TreeNode node in nodes )
					{
						node.Collapse( true );
					}
				}
				) );
			this.dirContextMenu.Items.Add( new ToolStripMenuItem(
				"Expand All",
				null,
				( eventSender, eventArgs ) => this.directoryTreeView.ExpandAll()
				) );
			this.dirContextMenu.Items.Add( new ToolStripSeparator() );
			this.dirContextMenu.Items.Add( new ToolStripMenuItem(
				"Check All",
				this.imageList_general.Images["tick.png"],
				( eventSender, eventArgs ) => this.toolStripButton_checkAllDirectories.PerformClick()
				) );
			this.dirContextMenu.Items.Add( new ToolStripMenuItem(
				"Uncheck All",
				null,
				( eventSender, eventArgs ) => this.directoryTreeView.SetCheckAll( false )
				) );
			this.dirContextMenu.Items.Add( new ToolStripMenuItem(
				"Clear folders",
				this.imageList_general.Images["folder_delete.png"],
				( eventSender, eventArgs ) => this.toolStripButton_clearAllDirectories.PerformClick()
				) );

			int addedIndex = this.fileContextMenu.Items.Add( new ToolStripMenuItem(
				"Open",
				this.imageList_general.Images["folder_page.png"],
				( eventSender, eventArgs ) =>
				{
					if( this.itemListView.SelectedItemList.Count == 1 )
					{
						System.Diagnostics.Process.Start( this.itemListView.SelectedItemList[0].CombiPath );
					}
				},
				"OpenFile"
				) );
			var item = this.fileContextMenu.Items[addedIndex];
			item.Font = new Font( item.Font, FontStyle.Bold );
			this.fileContextMenu.Items.Add( new ToolStripMenuItem(
				"Open with explorer",
				this.imageList_general.Images["folder.png"],
				( eventSender, eventArgs ) =>
				{
					if( this.itemListView.SelectedItemList.Count == 1 )
					{
						System.Diagnostics.Process.Start( "EXPLORER.EXE", "/select,\"" + this.itemListView.SelectedItemList[0].CombiPath + "\"" );
					}
				},
				"OpenExplorer"
				) );
			this.fileContextMenu.Items.Add( new ToolStripSeparator() );
			this.fileContextMenu.Items.Add( new ToolStripMenuItem(
				"Copy infomation to clipboard",
				this.imageList_general.Images["page_copy.png"],
				( eventSender, eventArgs ) => this.toolStripButton_copyFiles.PerformClick()
				) );

			// コマンドライン引数に指定されたパスを反映
			if( InitialPaths.Length > 0 )
			{
				this.AddDirectory( InitialPaths );
			}
		}

		void directoryTreeView_NodeClicked( object sender, TreeNodeMouseClickEventArgs e )
		{
			var node = (DirectoryTreeNode)e.Node;
			string treePath = DirectoryTreeNode.GetTreePath( node.DirectoryCombiPath );

			int foundIndex = this.itemListView.ViewItemList.FindIndex( ( item ) =>
				item.IsDirectory && DirectoryTreeNode.GetTreePath( item.CombiPath ) == treePath );

			if( foundIndex != -1 )
			{
				// 一度表示されていないとTopItemが反映されないので、
				// まずEnsureVisibleしておく
				this.itemListView.EnsureVisible( foundIndex);
				this.itemListView.TopItem = this.itemListView.ViewItemList[foundIndex];
			}
		}

		void itemListView_LoadingFinished( object sender, EventArgs e )
		{
		}

		void itemListView_ProgressChanged( object sender, ItemListView.ProgressChangedEventArgs e )
		{
			this.toolStripProgressBar1.Minimum = e.Min;
			this.toolStripProgressBar1.Maximum = e.Max;
			this.toolStripProgressBar1.Value = e.Current;
			this.toolStripStatusLabel_progress.Text = e.Current + "/" + e.Max;

			if( e.Max < 100 )
			{
				// 少ししかない時はプログレスバーの表示を省略
				this.HideProgressInfo();
				return;
			}

			if( e.State == ItemListView.ProgressChangedEventArgs.ProgressState.Working )
			{
				if( !this.toolStripProgressBar1.Visible )
				{
					this.itemListView.ForeColor = SystemColors.GrayText;
					this.ShowProgressInfo();
					workingStartedTime = DateTime.Now;
				}
			}
			else
			{
				// 行数情報の読み込みが終わった
				this.itemListView.ForeColor = SystemColors.WindowText;

				if( e.State == ItemListView.ProgressChangedEventArgs.ProgressState.Finished )
				{
					// 少し遅らせてプログレスバーを消す
					this.HideProgressInfo( true );
					this.toolStripStatusLabel_progress.Text = (DateTime.Now - workingStartedTime).TotalMilliseconds.ToString( "0" ) + " msec.";
				}
				else
				{
					// キャンセルした時はすぐに消す
					this.HideProgressInfo();
				}
			}
		}

		private void toolStripButton_checkAllDirectories_Click( object sender, EventArgs e )
		{
			this.directoryTreeView.SetCheckAll( true );
		}

		void directoryTreeView_MouseClick( object sender, MouseEventArgs e )
		{
			if( e.Button == MouseButtons.Right )
			{
				var pt = ((Control)sender).PointToScreen( e.Location );
				this.directoryTreeView.SelectedNode = this.directoryTreeView.GetNodeAt( e.Location );
				this.dirContextMenu.Show( pt );
			}
		}

		void progressBarTimer_Tick( object sender, EventArgs e )
		{
			this.HideProgressInfo();
		}

		private void notifyMessageTimer_Tick( object sender, EventArgs e )
		{
			this.HideNotifyMessageToStatusInfo();
		}

		private void itemListView_MouseClick( object sender, MouseEventArgs e )
		{
			if( e.Button == MouseButtons.Right )
			{
				var pt = ((Control)sender).PointToScreen( e.Location );
				this.fileContextMenu.Items["OpenFile"].Enabled = (this.itemListView.SelectedItemList.Count == 1);
				this.fileContextMenu.Items["OpenExplorer"].Enabled = (this.itemListView.SelectedItemList.Count == 1);
				this.fileContextMenu.Show( pt );
			}
		}

		void itemListView_MouseDoubleClick( object sender, MouseEventArgs e )
		{
			if( e.Button == MouseButtons.Left )
			{
				var item = (CountableItem)this.itemListView.GetItemAt( e.X, e.Y );
				if( item != null )
				{
					System.Diagnostics.Process.Start( item.CombiPath );
				}
			}
		}

		private string searchWord;
		void directoryTreeView_KeyDown( object sender, KeyEventArgs e )
		{
			if( char.IsLetterOrDigit( (char)e.KeyValue ) )
			{
				this.searchWord += e.KeyCode.ToString().ToLower();
				this.ShowNotifyMessageToStatusInfo( this.searchWord );

				e.SuppressKeyPress = true;
				var currentNode = this.directoryTreeView.SelectedNode;
				if( currentNode == null )
				{
					currentNode = this.directoryTreeView.TopNode;
				}

				// マッチしなくなったら次を探す
				if( !currentNode.Text.StartsWith( this.searchWord ) )
				{
					while( true )
					{
						if( currentNode.Nodes.Count > 0 )
						{
							currentNode = currentNode.Nodes[0];
						}
						else
						{
							while( currentNode != null && currentNode.NextNode == null )
							{
								currentNode = currentNode.Parent;
							}
							if( currentNode == null )
							{
								break;
							}
							currentNode = currentNode.NextNode;
						}

						if( currentNode.Text.StartsWith( this.searchWord, StringComparison.OrdinalIgnoreCase ) )
						{
							this.directoryTreeView.SelectedNode = currentNode;
							break;
						}
					}
				}
			}
			else
			{
				this.searchWord = string.Empty;
				this.HideNotifyMessageToStatusInfo();
			}
		}

		private void itemListView_KeyDown( object sender, KeyEventArgs e )
		{
			if( e.KeyCode == Keys.A && e.Control )
			{
				this.toolStripButton_selectAllFile.PerformClick();
			}
			if( e.KeyCode == Keys.C && e.Control )
			{
				this.toolStripButton_copyFiles.PerformClick();
			}
			if( e.KeyCode == Keys.F5 )
			{
				this.toolStripButton_refleshFiles.PerformClick();
			}
		}

		private void directoryTreeView_NodeMouseDoubleClick( object sender, TreeNodeMouseClickEventArgs e )
		{
			//TODO: ツリーノードをダブルクリックしたら
			// そのディレクトリに属するファイルを選択状態にしてファイルリストの一番上に表示したい。
			// 仮想モードの時の表示位置変更の方法がわからないので一旦保留。
			//string combiPath = this.directoryTreeView.GetFullPath( e.Node );
			//var item = this.itemListView.FindFirstItemWithDirPath( combiPath );
		}

		private void itemListView_SelectionChanged( object sender, ListViewVirtualItemsSelectionRangeChangedEventArgs e )
		{
			// 選択されているファイルを含むディレクトリの強調表示
			// 複数選択されている時は強調表示なし(選択数が多い時の速度に課題)
			if( this.itemListView.SelectedItemList.Count == 1 )
			{
				string combiPath = this.itemListView.SelectedItemList[0].CombiPath;
				string directoryCombiPath = this.itemListView.SelectedItemList[0].IsDirectory ?
					combiPath :
					Path.GetDirectoryName( combiPath );
				this.directoryTreeView.HighlightNode( directoryCombiPath );
			}
			else
			{
				this.directoryTreeView.HighlightNode( null );
			}

			//TODO: funcList更新

			this.UpdateStatusInfo();
		}

		private void directoryTreeView_DirectoryNumChanged( object sender, EventArgs e )
		{
			this.UpdateStatusInfo();
		}

		private void directoryTreeView_CheckedNodeChanged( object sender, EventArgs e )
		{
			this.itemListView.SetDirectories( this.directoryTreeView.CheckedDirectoryPaths );
			this.UpdateStatusInfo();
		}

		private void FormMain_DragDrop( object sender, DragEventArgs e )
		{
			if( e.Data.GetDataPresent( DataFormats.FileDrop ) )
			{
				string[] paths = (string[])e.Data.GetData( DataFormats.FileDrop, false );
				this.AddDirectory( paths );
			}
		}

		private void FormMain_DragEnter( object sender, DragEventArgs e )
		{
			if( e.Data.GetDataPresent( DataFormats.FileDrop ) )
			{
				e.Effect = DragDropEffects.Copy;
			}
			else
			{
				e.Effect = DragDropEffects.None;
			}
		}

		private void toolStripButton_clearDirectory_Click( object sender, EventArgs e )
		{
			if( this.directoryTreeView.DirectoryNum <= 0 )
			{
				return;
			}
			var result = MessageBox.Show( "Are you sure you want to clear the directories?", this.Text, MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2 );
			if( result == DialogResult.OK )
			{
				this.directoryTreeView.ClearDirectories();
			}
		}

		private void toolStripButton_selectAllFile_Click( object sender, EventArgs e )
		{
			this.itemListView.SelectAll();
			this.itemListView.Select();
		}

		private void toolStripButton_copyFiles_Click( object sender, EventArgs e )
		{
			this.CopySelectedItemToClipboard();
		}

		private void toolStripButton_refleshFiles_Click( object sender, EventArgs e )
		{
			this.RefreshFileList();
		}

		private void toolStripMenuItem_about_Click( object sender, EventArgs e )
		{
			var aboutForm = new AboutFrom();
			aboutForm.ShowDialog();
		}

		private void UpdateStatusInfo()
		{
			// 割り込み表示中なら中断
			this.HideNotifyMessageToStatusInfo();

			var infoText = new StringBuilder();

			int directoryNum = this.itemListView.DirectoryItemList.Count;
			int selectedDirectoryNum = this.itemListView.SelectedItemList.Count( ( item ) => item.IsDirectory );
			if( directoryNum > 0 )
			{
				if( selectedDirectoryNum > 0 )
				{
					infoText.AppendFormat( "{0}/", selectedDirectoryNum );
				}
				infoText.AppendFormat( "{0} folders", directoryNum );
			}
			if( this.itemListView.ViewItemList.Count > 0 )
			{
				infoText.Append( ", " );
				int selectedFileNum = this.itemListView.SelectedItemList.Count - selectedDirectoryNum;
				if( selectedFileNum > 0 )
				{
					infoText.AppendFormat( "{0}/", selectedFileNum );
				}
				int fileNum = this.itemListView.ViewItemList.Count - directoryNum;
				infoText.AppendFormat( "{0} files.", fileNum );
			}

			this.toolStripStatusLabel_info.Text = infoText.ToString();

			var countInfoText = new StringBuilder();
			if( this.itemListView.SelectedItemList.Count > 0 )
			{
				// 選択中ファイルの行数情報
				var countData = new CountData();
				foreach( CountableItem item in this.itemListView.SelectedItemList )
				{
					countData += item.CountData;
				}

				countInfoText.AppendFormat(
					"All:{0}  Enable:{1}  Blank:{2}  Comment:{3}  Effective:{4}",
					countData.AllLines.ToString( "#,0" ),
					countData.EnableLines.ToString( "#,0" ),
					countData.BlankLines.ToString( "#,0" ),
					countData.CommentLines.ToString( "#,0" ),
					countData.EffectiveLines.ToString( "#,0" )
					);
				this.toolStripStatusLabel_countInfo.Text = countInfoText.ToString();
				this.toolStripStatusLabel_countInfo.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
			}
			else
			{
				this.toolStripStatusLabel_countInfo.DisplayStyle = ToolStripItemDisplayStyle.None;
			}

		}

		private void ClearAllCheck( object sender, EventArgs e )
		{
			;
		}

		private void CopySelectedItemToClipboard()
		{
			// 選択されていなければ何もしない
			if( this.itemListView.SelectedItemList.Count <= 0 )
			{
				return;
			}

			// 出力項目名リスト作る
			var columnNameList = new List<string>();
			foreach( ColumnHeader column in this.itemListView.Columns )
			{
				if( column.Name != "Ext" )
				{
					columnNameList.Add( column.Name );
				}
			}

			//TODO: 表示列に限定した行数情報の提供はFileListViewがやるようにする
			var text = new StringBuilder();
			foreach( string name in columnNameList )
			{
				text.Append( name + "\t" );
			}
			text.Remove( text.Length - 1, 1 );
			text.AppendLine();

			var propertyNames = columnNameList.GetRange( 1, columnNameList.Count() - 1 ).ToArray();
			var totalCountData = new CountData();

			foreach( var item in this.itemListView.SelectedItemList )
			{
				text.Append( Path.GetFullPath( item.CombiPath ) + "\t" );
				text.Append( MainForm.CountDataToText( item.CountData, propertyNames ) );
				text.AppendLine();
				if( !item.IsDirectory )
				{
					totalCountData += item.CountData;
				}
			}
			text.Append( "total:\t" );
			text.Append( MainForm.CountDataToText( totalCountData, propertyNames ) );
			text.AppendLine();

			Clipboard.SetData( DataFormats.Text, text.ToString() );

			this.ShowNotifyMessageToStatusInfo( this.itemListView.SelectedItemList.Count + " information was copied to clipboard." );
		}

		private void RefreshFileList()
		{
			// 見た目でわかりやすいように一旦リストを空にする
			this.itemListView.SetDirectories( null );
			this.itemListView.SetDirectories( this.directoryTreeView.CheckedDirectoryPaths );
		}

		private void ShowNotifyMessageToStatusInfo( string message, int duration = 3000 )
		{
			if( !this.notifyMessageTimer.Enabled )
			{
				this.statusInfoTextBackup = this.toolStripStatusLabel_info.Text;
			}
			this.toolStripStatusLabel_info.Text = message;
			this.toolStripStatusLabel_info.Image = this.imageList_general.Images["information.png"];
			this.notifyMessageTimer.Enabled = false;
			this.notifyMessageTimer.Interval = duration;
			this.notifyMessageTimer.Enabled = true;
		}

		private void HideNotifyMessageToStatusInfo()
		{
			if( this.notifyMessageTimer.Enabled )
			{
				this.toolStripStatusLabel_info.Text = this.statusInfoTextBackup;
				this.toolStripStatusLabel_info.Image = null;
			}
			this.notifyMessageTimer.Enabled = false;

			//TODO: 本当はここではやりたくない、検索ワード専用のタイムアウトを用意すべき
			this.searchWord = string.Empty;
		}

		private void ShowProgressInfo()
		{
			this.toolStripProgressBar1.Visible = true;
			this.toolStripStatusLabel_progress.Visible = true;
			this.progressBarTimer.Enabled = false;
		}

		private void HideProgressInfo( bool delay = false )
		{
			if( delay )
			{
				this.progressBarTimer.Enabled = true;
			}
			else
			{
				this.itemListView.ForeColor = SystemColors.WindowText;
				this.toolStripProgressBar1.Visible = false;
				this.toolStripStatusLabel_progress.Visible = false;
				this.progressBarTimer.Enabled = false;
			}
		}
	}
}
