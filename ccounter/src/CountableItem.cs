﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ccounter
{
	abstract class CountableItem : ListViewItem
	{
		//
		// Public properties
		//

		public int ItemIndex { get; protected set; }
		public string CombiPath { get; protected set; }
		public CountData CountData { get { return this.countData.GetValueOrDefault(); } }
		public bool IsCounted { get { return this.countData.HasValue; } }
		public bool IsDirectory { get; protected set; }
		public abstract void CountItemContent();

		// Public methods

		public void SetCountData( CountData countData )
		{
			this.countData = countData;
		}

		public void ReflectCountDataToControl()
		{
			if( this.CountData != this.reflectedCountdata )
			{
				this.SubItems[ItemListView.FindColumnParameter( "All" ).Index].Text = this.CountData.AllLines.ToString( "#,0" );
				this.SubItems[ItemListView.FindColumnParameter( "Enable" ).Index].Text = this.CountData.EnableLines.ToString( "#,0" );
				this.SubItems[ItemListView.FindColumnParameter( "Blank" ).Index].Text = this.CountData.BlankLines.ToString( "#,0" );
				this.SubItems[ItemListView.FindColumnParameter( "Comment" ).Index].Text = this.CountData.CommentLines.ToString( "#,0" );
				this.SubItems[ItemListView.FindColumnParameter( "Effective" ).Index].Text = this.CountData.EffectiveLines.ToString( "#,0" );

				this.reflectedCountdata = this.CountData;
			}
		}

		public void AddCountDataToParents( CountData countdata )
		{
			if( this.parentItem != null )
			{
				this.parentItem.countData += countdata;
				this.parentItem.AddCountDataToParents( countdata );
			}
		}

		//
		// Protected fields
		//

		protected CountData? reflectedCountdata;
		protected CountableItem parentItem;
		protected CountData? countData;

		//
		// Protected methods
		//

		protected CountableItem() { }
	}

	class DirectoryItem : CountableItem
	{
		//
		// Public methods
		//

		public DirectoryItem( int itemIndex, string combiPath, DirectoryItem parent )
			: base()
		{
			string treePathForDisplay = DirectoryTreeNode.GetTreePath( combiPath ).Replace( "\\", "/ " );
			var items = new string[]
			{
				treePathForDisplay, "", "-", "-", "-", "-", "-"
			};
			this.ItemIndex = itemIndex;
			this.SubItems[0].Text = items[0];
			this.SubItems.AddRange( items.Skip( 1 ).ToArray() );
			this.CombiPath = combiPath;
			this.ImageIndex = 6;	// Directory icon
			this.parentItem = parent;
			this.IsDirectory = true;
		}

		public override void CountItemContent()
		{
			this.countData = new CountData();
		}
	}

	class FileItem : CountableItem
	{
		//
		// Public methods
		//

		public FileItem( int itemIndex, string combiPath, string ext, DirectoryItem parent )
			: base()
		{
			string treePathForDisplay = DirectoryTreeNode.GetTreePath( combiPath ).Replace( "\\", "/ " );
			var items = new string[]
			{
				treePathForDisplay, ext, "-", "-", "-", "-", "-"
			};
			this.ItemIndex = itemIndex;
			this.SubItems[0].Text = items[0];
			this.SubItems.AddRange( items.Skip( 1 ).ToArray() );
			this.CombiPath = combiPath;
			this.ImageIndex = 1;	// File icon //TODO: 拡張子によってイメージを変える
			this.parentItem = parent;
			this.IsDirectory = false;
		}

		public override void CountItemContent()
		{
			this.countData = CodeCounter.Analyze( this.CombiPath );
		}
	}
}
