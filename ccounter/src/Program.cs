﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text;

namespace ccounter
{
	static class Program
	{
		/// <summary>
		/// アプリケーションのメイン エントリ ポイントです。
		/// </summary>
		[STAThread]
		static void Main()
		{
			var argList = Environment.GetCommandLineArgs().ToList();
			argList.RemoveAt( 0 );
			var optionList = argList.FindAll( ( arg ) => arg.StartsWith( "-" ) );
			argList.RemoveAll( ( arg ) => optionList.Contains( arg ) );

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault( false );

			var form = new MainForm();
			if( optionList.Contains( "-c" ) )
			{
				PrintCountData( argList.ToArray() );
			}
			else
			{
				form.InitialPaths = argList.ToArray();
				Application.Run( form );
			}
		}

		static void PrintCountData( string[] paths )
		{
			using( var writer = Console.Out )
			{
				string[] propertyNames = { "All", "Enable", "Blank", "Comment", "Effective" };

				var headerText = new StringBuilder();
				headerText.Append( "Path" );
				foreach( string name in propertyNames )
				{
					headerText.Append( "\t" + name );
				}
				writer.WriteLine( headerText );

				var filePathList = new List<string>();
				foreach( string path in paths )
				{
					if( File.Exists( path ) )
					{
						filePathList.Add( path );
					}
					else if( Directory.Exists( path ) )
					{
						// パスが区切り文字で終わっていたらそのディレクトリ直下のファイルのみが対象
						bool recursive = !path.EndsWith( Path.DirectorySeparatorChar.ToString() );
						foreach( var filePath in Util.GetFilesInDirectory( path, @".*\.(c|cpp|cxx|h|hpp|hxx)$", null, recursive ) )
						{
							filePathList.Add( filePath );
						}
					}
				}

				var totalCountData = new CountData();
				foreach( string path in filePathList )
				{
					var countData = CodeCounter.Analyze( path ).GetValueOrDefault();
					string text = path + '\t' + MainForm.CountDataToText( countData, propertyNames );
					writer.WriteLine( text );
					totalCountData += countData;
				}
				writer.WriteLine( "total:\t" + MainForm.CountDataToText( totalCountData, propertyNames ) );
				writer.Flush();
			}
		}
	}
}
