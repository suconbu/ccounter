﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ccounter
{
	public partial class AboutFrom : Form
	{
		public AboutFrom()
		{
			InitializeComponent();
		}

		private void AboutFrom_Load( object sender, EventArgs e )
		{
			Util.TraverseControls( this, ( c ) => c.Font = SystemFonts.MessageBoxFont );

			this.uiVersionInfo.Text =
				Util.GetApplicationName() + "\n" +
#if DEBUG
				"version " + Util.GetVersionString() + "\n" +
#else
				"version " + Util.GetVersionString( 3 ) + "\n" +
#endif
				"\n" +
				Util.GetCopyrightString();

			this.uiChangeLog.Text =
				"2015-03-03 version 0.1.0\r\n" +
				"    - Trial version.";

			this.uiOkButton.Select();
		}

		private void uiOkButton_Click( object sender, EventArgs e )
		{
			this.Close();
		}
	}
}
